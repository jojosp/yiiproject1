drop database if exists project1;
create database project1;
use project1;


create table roles(
	role_id  int auto_increment primary key,
    rolename varchar(20)
);

insert into roles(rolename) values
('admin'),
('user');

create table users(
	user_id int auto_increment primary key,
    username varchar(20),
    pass varchar(255),
    email varchar(20),
    active boolean,
    role_id int,
	created int,
    createdby int,
	modified int ,
    modifiedby int,
	CONSTRAINT FK FOREIGN KEY(role_id) REFERENCES roles(role_id)
);


create table users_info(
	user_id int primary key,
    firstname varchar(20),
    lastname varchar(20)
	);


