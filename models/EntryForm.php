<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;


class EntryForm extends Users
{
    public $role;
    public $user_id;
    public $image;
    public $name;
    public $ext;

    public function rules()
    {
        return 
            array_merge(
                parent::rules(),
                [
                    [['role', 'user_id',], 'required'],
                    [['image'], 'safe'],
                    [['image'], 'file', 'extensions'=>'jpg, gif, png'],
                    [['image'], 'file', 'maxSize'=>'1000000','skipOnEmpty' => false],
                    
               ]
            );
    
    }

    public function upload()
    {
        if ($this->validate()) 
        {
           // $path = Yii::$app->basePath . '/web/images/';
            //$this->image->saveAs('uploads/' . $this->image->baseName . '.' . $this->image->extension);
            //$this->image->saveAs($path);
           // $this->image->saveAs(Yii::$app->basePath . $this->image->baseName . '.' . $this->image->extension);
            return true;
        } else 
        {
            return false;
        }
    }

}