<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class RegisterForm extends Users
{
    public $username;
    public $password;
    public $email;
    public $role;
    public $fname;
    public $lname;
    public $active=false;
    public $password_hash;


    public function rules()
    {
        return [
            [['username', 'password','email','role','fname','lname'], 'required'],
            [['username', 'password','email','fname','lname',], 'trim'],
            ['email', 'email'],
            ['email', 'unique'],
            ['username', 'unique'],
            //['username', 'checkUniqueUsername'],
            //['email', 'checkUniqueEmail'],
            ['active', 'boolean'],
            ['password', 'setPassword'],
            ['password', 'match', 'pattern'=>'/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/', 'message'=>'Password must contain at least one uppercase,lowercase character and a number and length>=6.']
        ];
    }

    public function attributeLabels() 
    {
        return [
           'username' => 'Username',
           'password' => 'Password',
           'email' => 'Email',
           'role' => 'Role',
           'fname' => 'FirstName',
           'lname' => 'LastName',
           'active' => 'Active'
        ];
     }

    //  public function validatePassword($password)
    //  {
    //      return Yii::$app->security->validatePassword($password, $this->$password_hash);
    //  }

     public function setPassword($password)
     {
        return $this->password_hash = Yii::$app->security->generatePasswordHash($password);
     }

    //  public function checkUniqueUsername($attribute, $params, $validator)
    //  {
    //     $users_names = Users::find()
    //     ->where(['username'=> $attribute])
    //     ->asArray()
    //     ->all();
 

    //     if (in_array($this->$attribute, $users_names)) 
    //     {
    //         $this->addError($attribute, 'Username is taken.');
            
    //     }
    //     // $user = Users::findOne(['username'=> $attribute]);
    //     // if (!$user) {
    //     //     return true;
    //     // }
    //     // return false;
    //  }

    //  public function checkUniqueEmail($attribute, $params, $validator)
    //  {
    //     $users_emails = Users::find()
    //     ->where(['email'=> $attribute])
    //     ->asArray()
    //     ->all();

    //     if (in_array($this->$attribute, $users_emails)) 
    //     {
    //         $this->addError($attribute, 'Email already in use.');
    //         return false;
    //     }else return true;

    //     // $user = Users::find()
    //     // ->where(['email'=> $email])->one();
    //     // if (!$user) {
    //     //     return true;
    //     // }
    //     // return false;
    //  }
}

