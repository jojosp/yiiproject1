<?php

namespace app\models;
use yii\web\IdentityInterface;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use app\models\UsersInfo;

/**
 * This is the model class for table "users".
 *
 * @property int $user_id
 * @property string $username
 * @property string $pass
 * @property string $email
 * @property int $active
 * @property int $role_id
 * @property string $created
 * @property string $createdby
 * @property string $modified
 * @property string $modifiedby
 *
 * @property Roles $role
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{
    public $password_hash;
    public $firstname;
    public $lastname;
    public $img_src;
    public $adminusers = array();
    public $simpleusers = array();
    // public $authKey;
    // public $accessToken;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['active', 'role_id'], 'integer'],
            //[['created', 'modified'], 'safe'],
            [['username', 'pass', 'email'], 'string', 'max' => 255],
            [['modifiedby', 'createdby'], 'integer'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Roles::className(), 'targetAttribute' => ['role_id' => 'role_id']],
            //[['image'], 'safe'],
            //[['img_src'], 'file', 'extensions'=>'jpg, pdf, png', 'skipOnEmpty' => false],
            //[['img_src'], 'file', 'maxSize'=>'1000000'],
            [['img_src',], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => 'Username',
            'pass' => 'Pass',
            'email' => 'Email',
            'active' => 'Active',
            'role_id' => 'Role ID',
            'img_src' =>  'File upload',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Roles::className(), ['role_id' => 'role_id']);
    }
//------------------------------------------------------------------------------------------------

    public function setPassword($password)
    {
        return $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    public function getUsersinfo(){
        return $this->hasOne(UsersInfo::classname(),['user_id' => 'user_id']);
    }


//----------------------------------------------------------------------------------------------------






    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($user_id)
    {
        return static::findOne($user_id);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }


    public static function findRoleSpec($username)
    {
        return static::find()
        ->where(['username' => $username ])
        ->one();

    }


    public static function findActives()
    {
        return $activeusers = Users::findAll([
            'active' => 1
        ]);

    }

    public static function findAdmins()
    {
        $data = Users::find()->where([
            'role_id' => 1
        ])->select(['user_id as id','user_id as name'])->asArray()->all();
        
            return $data;

    }
    public static function findUsers()
    {
        $data = Users::find()->where([
            'role_id' => 2
        ])->select(['user_id as id','user_id as name'])->asArray()->all();

        return $data;

    }

    public static function findInactives()
    {
        return $inactiveusers = Users::findAll([
            'active' => 0
        ]);
        
    }

    public static function findByRole($role_id)
    {
        return static::findAll()
        ->where(['role_id' => $role_id ]);

    }


    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->user_id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function getRole2()
    {
        return $this->role_id;
    }


    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function behaviors()
    {
             return [
                //  'timestamp' => [
                //      'class' => 'yii\behaviors\TimestampBehavior',
                //      'attributes' => [
                //          //ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                //          ActiveRecord::EVENT_AFTER_UPDATE => ['modified'],
                //      ],
                //  ],
                //  [
                //     'class' => BlameableBehavior::className(),
                //     'updatedByAttribute' => 'modifiedby'
                // ],
                [
                    'class' => BlameableBehavior::className(),
                    'createdByAttribute' => 'createdby',
                    'updatedByAttribute' => 'modifiedby',
                ],
                'timestamp' => [
                    'class' => 'yii\behaviors\TimestampBehavior',
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],
                        ActiveRecord::EVENT_BEFORE_UPDATE => ['modified'],
                    ],
                ],
             ];
    }

    
}
