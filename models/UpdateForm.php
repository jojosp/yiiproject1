<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class UpdateForm extends Model
{
    public $user_id;
    public $username;
    public $password;
    public $email;
    public $role;
    public $fname;
    public $lname;
    public $active=false;
    public $password_hash;


    public function rules()
    {
        return 
        [
            [['username', 'password','email','role','fname','lname'], 'required'],
            [['username', 'password','email','fname','lname',], 'trim'],
            ['email', 'email'],
            //['email', 'unique'],
            //['username', 'unique'],
            ['active', 'boolean'],
            ['password', 'setPassword'],
            ['password', 'match', 'pattern'=>'/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/', 'message'=>'Password must contain at least one uppercase,lowercase character and a number.'],
            //['username', 'exist', 'targetClass' => Users::class, 'targetAttribute' => ['username' => 'username']]
        
        ];
    }

    public function attributeLabels() 
    {
        return [
           'username' => 'Username',
           'password' => 'Password',
           'email' => 'Email',
           'role' => 'Role',
           'fname' => 'FirstName',
           'lname' => 'LastName',
           'active' => 'Active'
        ];
     }

    //  public function validatePassword($password)
    //  {
    //      return Yii::$app->security->validatePassword($password, $this->$password_hash);
    //  }

    //  public function setPassword($password)
    //  {
    //      $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    //  }
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

}