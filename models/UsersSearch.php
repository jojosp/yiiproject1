<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSearch represents the model behind the search form of `app\models\Users`.
 */
class UsersSearch extends Users
{

    public $firstname;
    public $lastname;
    public $rolename;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'active', 'role_id' , 'created', 'createdby', 'modified', 'modifiedby'], 'integer'],
            [['username', 'pass', 'email', 'firstname', 'rolename', 'lastname'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find()->joinWith('usersinfo ui');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'active' => $this->active,
            'role_id' => $this->role_id,
            'created' => $this->created,
            'createdby' => $this->createdby,
            'modified' => $this->modified,
            'modifiedby' => $this->modifiedby,
            
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
        ->andFilterWhere(['like', 'pass', $this->pass])
        ->andFilterWhere(['like', 'ui.firstname', $this->firstname])
        ->andFilterWhere(['like', 'ui.lastname', $this->lastname])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
