<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Users
{
    public $username;
    public $pass;
    

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'pass'], 'required'],
            // password is validated by validatePassword()
            //['pass', 'validatePassword'],
            [['username'], 'exist','targetClass'=>'\app\models\Users','message'=>'Incorrect username or password.'],
            //[['pass'], 'exist','targetClass'=>'\app\models\Users','message'=>'Incorrect username or password.'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    // public function validatePassword($attribute, $params)
    // {
    //     $password_hash = Yii::$app->security->generatePasswordHash($attribute);
        
    //     if (!$this->hasErrors()) {
    //         $user = $this->getUser();

    //         if(!Users::findOne([
    //             'username' => $this->username,
    //             'pass' => $password_hash
    //         ])){
    //             $this->addError($attribute, 'Incorrect dsdusername or password.');
    //         }
    //     }
    // }
    public function validatePassword($password)
    {
       // $pass = Yii::$app->getSecurity()->generatePasswordHash($password);
        return Yii::$app->getSecurity()->validatePassword($password, $this->pass);
    }

    public function validateUsername($attribute, $params)
    {
        
        if (!Users::find()->where(['username' => $atrribute])->exists()) 
        {
            $this->addError($attribute, 'Incorrect username or password.');
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser());
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Users::findByUsername($this->username);
        }

        return $this->_user;
    }
}
