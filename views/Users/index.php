<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\models\Users;



/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
$formatter = \Yii::$app->formatter;


?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>


<?php
    // yii\bootstrap\Modal::begin(['id' =>'modal']);
    // yii\bootstrap\Modal::end();
    Modal::begin([
        'header' => 'Info',
        'id' => 'mymodal',
        'size' => 'modal-lg',
        "footer"=>"",
    ]);
     
    echo '<h3>About user <span id="target"><span></h3>
    <form id="form">
    <label>Created by user with id: <p><input type="text"  id="id" value="" disabled/></p></label>
    <label>User username<p><input type="text"  id="username"disabled value=""/></p></label>
    <label>User email<p><input type="text"  id="email" value=""disabled/></p></label>
    <label>User firstname<p><input type="text"  id="firstname" value=""disabled/></p></label>
    <label>User lastname<p><input type="text"  id="lastname" value=""disabled/></p></label>
    <label>User role<p><input type="text"  id="role" value=""disabled/></p></label>
    <label>User active<p><input type="text"  id="active" value=""disabled/></p></label>
    </form>
    <div class="checkbox"  id="f">
        <label><input type="checkbox" value="" id="fsubmit">Change active status of user <span id="target2"><span></label>
        <p>user is currently ACTIVE: <span id="target3"><span></p>
    </div>';


    Modal::end();
?>
<?php 


?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => 
        [
            
            

            'user_id',
            'username',
            'pass',
            'email:email',
            //'active:boolean',
            [
                'attribute' => 'active',
                'value'=>function($model){
                     //<?= Html::tag('p', Html::encode($user->name), ['class' => 'hidden']) ;
                    return \Yii::$app->formatter->asBoolean($model->active);
                },
                'contentOptions' => function($model){
                    return ['id' => 'a_'.$model->user_id];
                },
                'filter' => ['1' => 'Yes', '0' => 'No'],
            ],
            [
                'attribute' => 'firstname',
                // 'value' => 'usersinfo.firstname',
                'value'=>function($model){
                    return $model->usersinfo->firstname;
                }
            ],
            [
                'attribute' => 'lastname',
                'value' => 'usersinfo.lastname'
            ],
            [
                'attribute' => 'role_id',
                'value' => 'role.rolename',
                'filter' => ['1' => 'Admin', '2' => 'User'],
            ],
            
        

            ['class' => 'yii\grid\ActionColumn',
              'header' => 'View',
             'template' => '{custom}',
             'buttons' => 
              [
                'custom' => function ($url, $model, $key) 
                {
                    

                    $i = Yii::$app->user->identity->user_id;
                    $current = Users::find()->where(['user_id' => $i])
                    ->one();
                    //echo $model->active;

                    $t = $model->createdby;
                    $creator = Users::findOne($t);

                    $hide_checkbox;
                    if($current->user_id === $model->user_id){
                        $hide_checkbox=0;
                    }else {$hide_checkbox=1;};
                    //echo '</br>'.($hide_checkbox);
                    //echo \Yii::$app->formatter->asBoolean($hide_checkbox);
                    if(!$hide_checkbox){ 
                        //echo 'i got in here';
                        //echo 'is not current user'.var_dump($model->user_id);
                        return Html::a('<span class=" glyphicon glyphicon-heart " ></span>', '#mymodal'
,['class' => 'mod_but btn btn-success'
,'data-toggle'=>'modal'
,'data-usr'=>$model->user_id
//,'data-usr2'=>$model->user_id
,'data-id'=>$current->createdby
,'data-username'=>$current->username
,'data-fname'=>$current->usersinfo->firstname
,'data-lname'=>$current->usersinfo->lastname
,'data-email'=>$current->email
,'data-active'=>\Yii::$app->formatter->asBoolean($current->active)
,'data-active2'=>\Yii::$app->formatter->asBoolean($model->active)
,'data-role'=>$current->role->rolename
,'data-cu'=>$hide_checkbox
]);

                                    }else
{
    //echo 'i got in there '.$model->createdby.\Yii::$app->formatter->asBoolean($model->active);
return Html::a('<span class=" glyphicon glyphicon-heart " ></span>', '#mymodal'
,['class' => 'mod_but btn btn-success'
,'data-toggle'=>'modal'
,'data-usr'=>$model->user_id
,'data-usr2'=>$model->user_id
,'data-id'=>$model->createdby
,'data-username'=>$creator->username
,'data-fname'=>$creator->usersinfo->firstname
,'data-lname'=>$creator->usersinfo->lastname
,'data-email'=>$creator->email
,'data-active'=>\Yii::$app->formatter->asBoolean($creator->active)
,'data-active2'=>\Yii::$app->formatter->asBoolean($model->active)
,'data-role'=>$creator->role->rolename
,'data-cu'=>$hide_checkbox
]);
}
                }
              ]
            ],
        ],
    ]); 
    
 ?>
 
</div>
