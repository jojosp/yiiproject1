<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form ActiveForm */
?>
<div class="user-reg-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'active') ?>
        <?= $form->field($model, 'role_id') ?>
        <?= $form->field($model, 'created') ?>
        <?= $form->field($model, 'modified') ?>
        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'pass') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'createdby') ?>
        <?= $form->field($model, 'modifiedby') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- user-reg-form -->
