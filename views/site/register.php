<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h2>Enter your info!</h2>
<?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'email')->input('email') ?>

    <?= $form->field($model, 'fname') ?>

    <?= $form->field($model, 'lname') ?>

    <?= $form->field($model, 'role')->dropDownList([1=>'admin',2=>'user'],['prompt'=>'Select Role']); ?>

    <?= $form->field($model, 'active')->checkBox(['active' => 'Active']) ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
  
<?php ActiveForm::end(); ?>
