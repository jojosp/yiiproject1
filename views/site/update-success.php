<?php
use yii\helpers\Html;


?>

<h2>You have successfully updated your info!</h2>
<p>You have entered the following information:</p>

<ul>
    <li><label>Username</label>: <?= Html::encode($model->username) ?></li>
    <li><label>Password</label>: <?= Html::encode($model->password) ?></li>
    <li><label>Email</label>: <?= Html::encode($model->email) ?></li>
    <li><label>FirstName</label>: <?= Html::encode($model->fname) ?></li>
    <li><label>LastName</label>: <?= Html::encode($model->lname) ?></li>
    <li><label>Role</label>: <?= Html::encode($model->role) ?></li>
    <li><label>Active</label>: <?= Html::encode($model->active) ?></li>
</ul>