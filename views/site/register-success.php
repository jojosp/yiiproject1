<?php
use yii\helpers\Html;
?>
<h2>You have successfully created a user!</h2>
<p>You have entered the following information:</p>

<ul>
    <li><label>Username</label>: <?= Html::encode($model->username) ?></li>
    <li><label>Password</label>: <?= Html::encode($model->password_hash) ?></li>
    <li><label>Email</label>: <?= Html::encode($model->email) ?></li>
    <li><label>FirstName</label>: <?= Html::encode($model->fname) ?></li>
    <li><label>LastName</label>: <?= Html::encode($model->lname) ?></li>
    <li><label>Role</label>: <?= Html::encode($model->role) ?></li>
    <li><label>Active</label>: <?= Html::encode($model->active) ?></li>
</ul>
<br>

<?= var_dump($errors) ?>


<!-- <ul>
    <li><label>User ID</label>: <?= Html::encode($user_model->user_id) ?></li>
    <li><label>Username</label>: <?= Html::encode($user_model->username) ?></li>
    <li><label>Password</label>: <?= Html::encode($user_model->pass ) ?></li>
    <li><label>Email</label>: <?= Html::encode($user_model->email) ?></li>
    <li><label>FirstName</label>: <?= Html::encode($user_info_model->firstname) ?></li>
    <li><label>LastName</label>: <?= Html::encode($user_info_model->lastname) ?></li>
    <li><label>Role</label>: <?= Html::encode($user_model->role_id) ?></li>
    <li><label>Created By</label>: <?= Html::encode($user_model->createdby) ?></li>
    <li><label>Active</label>: <?= Html::encode($user_model->active) ?></li>
</ul> -->