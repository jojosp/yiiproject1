<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$identity = Yii::$app->user->identity;
 $isGuest = Yii::$app->user->isGuest;
?>
<h2>You can update your info!</h2>
<?= var_dump($identity) ?>
<?= var_dump($isGuest) ?>

<?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'username')->textInput()->input('username') ?>

    <?= $form->field($model, 'password')->passwordInput()->hint('Please enter/or change your password again') ?>

    <?= $form->field($model, 'email')->input('email') ?>

    <?= $form->field($model, 'fname') ?>

    <?= $form->field($model, 'lname') ?>

    <?= $form->field($model, 'role')->dropDownList([1=>'admin',2=>'user'],['prompt'=>'Select Role']); ?>

    <?= $form->field($model, 'active')->checkBox(['active' => 'Active']) ?>

    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
    </div>
  
<?php ActiveForm::end(); ?>