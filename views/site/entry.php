<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use \kartik\depdrop\Depdrop;

use app\models\Roles;
use app\models\Users;
use yii\helpers\Url;
use yii\helpers\Json;
?>

<?php 
// $data=array();
//     $data = Users::find()->where([
//         'role_id' => 1
//     ])->select(['user_id as id','username as name'])->asArray()->all();
//     print_r($data[0]);
//     //$result = ArrayHelper::map($data, 'id', 'name');
//     echo '</br>';
//     echo Json::encode($data[0]);
//     echo '</br>';
//     $v = Json::encode($data[0]);
//     $s =  str_replace(':','=>',$v);
//     $g =  str_replace('"','\'',$s);
//     echo $g;
    //print_r(v);
    //print_r($result);
echo Yii::$app->basePath;

?>
<?php $form = ActiveForm::begin([
          'options'=>['enctype'=>'multipart/form-data']]); ?>

   <!-- $form->field($model, 'role')->dropDownList([1=>'admin',2=>'user'],['prompt'=>'Select Role']);  -->

   <?php 
    $roles=Roles::find()->all();
    $list=ArrayHelper::map($roles,'role_id','rolename');
    ?>
<!-- , ['prompt'=>'Select Role']     , ['role_id'=>'rolename']   -->
   <?= $form->field($model, 'role')->dropDownList($list, ['id'=>'roledrop','prompt'=>'Select Role']); ?>

    <?= $form->field($model, 'user_id')->widget(DepDrop::classname(), [
        //'options' => ['multiple' => true,],
        'pluginOptions'=>[
         'depends'=>['roledrop'],
         'placeholder' => 'Select...',
         'url' => Url::to(['/site/subcat']),
         ]
        ]);
    ?>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), [
              'options' => ['accept' => ['image/*','application/pdf'],'multiple' => false],
               'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png'],'showUpload' => false,],
          ]);   ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
<?php print_r($model->getErrors()); ?>