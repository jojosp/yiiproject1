-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2018 at 03:46 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project1`
--

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `rolename` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `rolename`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `img_src` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `pass`, `email`, `active`, `role_id`, `created`, `createdby`, `modified`, `modifiedby`, `img_src`) VALUES
(1, 'aaa', '$2y$13$dTS/VDZILWC/9kDaaNd4jeSpM/0XztAZW.i2VFebm6bmfR2brkjCu', 'aaaaa@kdkd.comk', 1, 1, 1532517326, 1, 1532517326, NULL, NULL),
(2, 'bbb', '$2y$13$xW3keA/jhsS.hoDIXYyq8Oe49ZAXTlboi/XD8cjWvdOs1k5w6iPj.', 'bbb@bbb.com', 0, 2, 1532517345, 1, 1532670616, 1, NULL),
(3, 'ccc', '$2y$13$GZuhfnci8w63NKQMtp1Wg.GcsLE2HgHJ/VDWejG440oQCJ./oZ1WO', 'ccc@kdkdd.com', 0, 1, 1532517406, 1, 1532670622, 1, NULL),
(4, 'ddd', '$2y$13$C5ymQp7o/55qAyAkdKoxiu.R8Z5u.MaLm2.vRKnOe.xHnbl9PcyS.', 'ddd@dddd.com', 0, 2, 1532517467, 4, 1532609006, 1, NULL),
(5, 'www', '$2y$13$vCpA0afmOMQ47d4n.XwepO3hH6HbqU.z33GrMmZzZDNup8i3cHvHm', '222@edrewfd.com', 0, 2, 1532604917, 1, 1532609599, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_info`
--

CREATE TABLE `users_info` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(20) DEFAULT NULL,
  `lastname` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_info`
--

INSERT INTO `users_info` (`user_id`, `firstname`, `lastname`) VALUES
(1, 'aaa', 'aaa'),
(2, 'bbb', 'bbb'),
(3, 'ccc', 'ccc'),
(4, 'ddd', 'ddd'),
(5, 'www', 'www');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `FK` (`role_id`);

--
-- Indexes for table `users_info`
--
ALTER TABLE `users_info`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
