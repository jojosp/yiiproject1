<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\EntryForm;
use app\models\RegisterForm;
use app\models\UpdateForm;
use app\models\Users;
use app\models\UsersInfo;
use app\models\Roles;
use yii\helpers\Json;

use yii\web\UploadedFile;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->pass = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    // public function actionContact()
    // {
    //     $model = new ContactForm();
    //     if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
    //         Yii::$app->session->setFlash('contactFormSubmitted');

    //         return $this->refresh();
    //     }
    //     return $this->render('contact', [
    //         'model' => $model,
    //     ]);
    // }

    /**
     * Displays about page.
     *
     * @return string
     */
    // public function actionAbout()
    // {
    //     return $this->render('about');
    // }

    // public function actionSay($message = 'Hello')
    // {
    //     return $this->render('say', ['message' => $message]);
    // }

    public function actionEntry()
    {
        $msg='ok';
        $model = new EntryForm();
        

        if ($model->load(Yii::$app->request->post())) {
            
            $model->image = UploadedFile::getInstance($model, 'image');
            $nimage = UploadedFile::getInstance($model, 'image');
            
            if ($model->upload()) {
                $usr = Users::findOne($model->user_id);
                // file is validated
                $usr->img_src = $nimage->basename. '.' . $nimage->extension;
                $path = Yii::$app->basePath . '/web/images/'. $nimage->baseName . '.' . $nimage->extension;
                //$nimage->saveAs(Yii::$app->basePath . $this->image->baseName . '.' . $this->image->extension);
                // $nimage->saveAs($path);
                if($nimage->extension=="jpg"){
                    // var_dump($nimage->extension);
                    $path = Yii::$app->basePath . '/web/images/jpg/'. $nimage->baseName . '.' . $nimage->extension;;
                    $nimage->saveAs($path);
                }
                
                // var_dump($nimage->extension);die();
                $usr->save();
                return $this->render('entry-confirm', ['msg' => $msg]);
            }
        }else return $this->render('entry', ['model' => $model,]);




        // return $this->render('upload', ['model' => $model]);

        // if ($model->load(Yii::$app->request->post()) ) 
        // {
            
        //     $usr = Users::findOne($model->user_id);
        //     $image = UploadedFile::getInstance($model, 'image');
        //     $usr->img_src = $image->name;
        //     $usr->save();

        //      if (!is_null($image)) 
        //     {
                
               
        //        $model->img_src = $image->name;
              
        //        $ext = end((explode(".", $image->name)));
        //        Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/images/';
        //        print_r($model->getErrors());die;

        //         if($ext=='jpg'){
        //             $path = Yii::$app->params['uploadPath'] .'/jpg/'. $image->name;
        //             $image->saveAs($path);
                 
                    
        //             print_r($usr->getErrors());die;

        //         }else if($ext=='png'){
                    
        //             $path = Yii::$app->params['uploadPath'] .'/png/'. $image->name;
        //             $image->saveAs($path);
                    
        //             print_r($usr->getErrors());die;
        //         }
                
                
        //         return $this->render('entry-confirm', ['msg' => $msg]);

        //     }
    
        // }return $this->render('entry', ['model' => $model]); 
}
                


        //     return $this->render('entry-confirm', []);
        // } else {
        //     // either the page is initially displayed or there is some validation error
        //     return $this->render('entry', ['model' => $model ]);
        // }
    

    public function actionRegister() 
    {
        $model = new RegisterForm();
        $user_model = new Users();
        $user_info_model = new UsersInfo();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // valid data received in $model
            //$count = Users::find()->count();
            //$bl = $model->checkUniqueUsername('aaa');

            $user_model->attributes = \Yii::$app->request->post('RegisterForm');
            //$user_model->user_id = ++$count;
            $user_model->pass = $model->password_hash;
            //$user_model->createdby = $model->user_id;
            $user_model->role_id = $model->role;
            $user_model->save();

            $currentusr = Users::findOne(['username'=>$model->username]);
            $c_id = $currentusr->user_id;
            if(Yii::$app->user->identity){
                $user_to_be_saved = Users::findOne($c_id);
                $user_model->createdby = Yii::$app->user->identity->user_id;
                $user_model->save();
            }else
            {
                $user_to_be_saved = Users::findOne($c_id);
                $user_model->createdby = $c_id;
                $user_model->save();
            }
            $errors=$model->getErrors();

            //$user_info_model = new UsersInfo();
            //$user_info_model->attributes = \Yii::$app->request->post('RegisterForm');
            $user_info_model->user_id = $user_model->user_id;
            $user_info_model->firstname = $model->fname;
            $user_info_model->lastname = $model->lname;
            $user_info_model->save();
            

            return $this->render('register-success', ['model' => $model, 'user_model'=>$user_model,'user_info_model'=>$user_info_model, 'errors'=>$errors ]);
        } else {
            // either the page is initially displayed or there is some validation error
            return $this->render('register', ['model' => $model]);
        }
    }

    public function actionUpdate() 
    {
        $model = new UpdateForm();
        $model->user_id = Yii::$app->user->identity->user_id;
        $model->username = Yii::$app->user->identity->username;
        $model->email = Yii::$app->user->identity->email;
        $model->role = Yii::$app->user->identity->role_id;
        $model->active = Yii::$app->user->identity->active;
        $inf = UsersInfo::findOne($model->user_id);
        $model->fname = $inf->firstname;
        $model->lname = $inf->lastname;


        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            // valid data received in $model
            $usr = Users::findOne($model->user_id);
            //$usr->user_id = $model->user_id;
            $usr->username = $model->username;
            $usr->pass = $model->password_hash;
            $usr->email = $model->email;
            $usr->role_id = $model->role;
            $usr->active = $model->active;
            $usr->save();

            $usrinf = UsersInfo::findOne($model->user_id);
            $usrinf->firstname = $model->fname;
            $usrinf->lastname = $model->lname;
            $usrinf->save();
            // do something meaningful here about $model ...
            //return $this->redirect(['site/update-success', 'model' => $model]);
            return $this->render('update-success', ['model' => $model]);
        } else {
            // either the page is initially displayed or there is some validation error
            $model->password = '';
            return $this->render('update', ['model' => $model]);
        }
    }

    public function actionSubcat() 
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) 
        {
            $parents = $_POST['depdrop_parents'];

            if ($parents != null) 
            {
                if($parents[0]==1)
                {
                    //$role_id = $parents[0];
                    $out = Users::findAdmins();
                    echo Json::encode(['output'=>$out, 'selected'=>'']);
                    return;
                }else if($parents[0]==2)
                {
                    $out = Users::findUsers();
                    echo Json::encode(['output'=>$out, 'selected'=>'']);
                    return;
                }
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
}
